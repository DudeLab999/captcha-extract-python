""" Automate script """
import time
import cv2 as cv
import numpy as np
import pytesseract as tesseract
from selenium import webdriver
tesseract_location = "A://OCR/tesseract.exe";
tesseract.pytesseract.tesseract_cmd = tesseract_location

# Prepare Driver
endpoint = "captchaurl"
driver = webdriver.Chrome()  # Optional argument, if not specified will search path.
driver.get(endpoint)
driver.maximize_window()

# Fill Username
search_username_box = driver.find_element_by_id('ctl00_cphForLogin_username')
time.sleep(1)
search_username_box.send_keys('XXXXX')

# Fill Password
search_password_box = driver.find_element_by_id('ctl00_cphForLogin_password')
time.sleep(1)
search_password_box.send_keys('XXXXXX')

# Submit button
submit_login_button = driver.find_element_by_id('ctl00_cphForLogin_lbtnLoginNew')
time.sleep(1)
submit_login_button.click()

# If Captcha Appear
captcha_image = driver.find_element_by_id('ctl00_cphForLogin_captcha')

# Save screenshot
# 1920 * 925 size
driver.save_screenshot('captcha1.png')
time.sleep(0.5)

# Prepare Image to show
img = cv.imread('captcha1.png')

# Crop only captcha 859,396 => Top Left, 1060,447 Bottom Right
crop_img_captcha = img[396:396+abs(396-447), 859:859+abs(859-1060)]


# Use crop image to extract captcha
gray = cv.cvtColor(crop_img_captcha, cv.COLOR_BGR2GRAY)
gray = cv.medianBlur(gray, 5)
blured = gray

cv.imwrite("blured_captcha.png", gray)

# Test Erode for comparing with other method
# Creating kernel 
kernel = np.ones((6, 6), np.uint8) 
  
# Using cv2.erode() method  
gray = cv.erode(gray, kernel)   #, cv.BORDER_REFLECT

cv.imwrite("crop_captcha.png", crop_img_captcha)
cv.imwrite("Eroded_captcha.png", gray)

cv.imshow("Crop_Image", crop_img_captcha)
# cv.imshow("OriginalImage", img)
cv.imshow("Blur", blured)
# cv.imshow("Output", gray)
cv.destroyAllWindows()



# cv.waitKey(0)

gray = cv.threshold(gray, 0, 255,
		cv.THRESH_BINARY | cv.THRESH_OTSU)[1]

text = tesseract.image_to_string(gray)
text = text.replace(" ", "")

print(text)

captcha_input_box = driver.find_element_by_id('ctl00_cphForLogin_tbInput')
captcha_input_box.send_keys(text)

# Confirm captcha
confirm_captcha_box = driver.find_element_by_id('ctl00_cphForLogin_btnSubmit')
